const tabs = document.querySelector(".tabs");
const tabsText = document.querySelectorAll(".tabs-content li");

addEventListener("click", (event) => {
	if (event.target.tagName === "LI") {
		event.target
			.closest("ul")
			.querySelector(".active")
			.classList.remove("active");
		event.target.classList.add("active");
		let text = event.target.dataset.name;

		document.querySelector(".active-text").classList.remove("active-text");
		document
			.querySelector(`[data-text = ${text}]`)
			.classList.add("active-text");
	}
});
